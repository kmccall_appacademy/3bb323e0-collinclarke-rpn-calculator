class RPNCalculator

 def initialize
   @stack = []
 end

 def push(num)
   @stack << num
 end

 def plus
   perform_operation(:+)
 end

 def value
   @stack.last
 end

 def minus
   perform_operation(:-)
 end

 def divide
   perform_operation(:/)
 end

 def times
   perform_operation(:*)
 end

 def tokens(string)
   elements = string.split
   elements.map { |el| operation?(el) ? el.to_sym : Integer(el)}
 end

def evaluate(string)
  tokens = tokens(string)
  tokens.each do |token|
    case token
    when Integer
      push(token)
    else
      perform_operation(token)
    end
  end
  value
end

 private

  def operation?(el)
    [:+, :-, :*, :/].include?(el.to_sym)
  end

  def perform_operation(op_symbol)
    raise "calculator is empty" if @stack.size < 2
    second_num = @stack.pop
    first_num = @stack.pop
   case op_symbol
     when :+
       @stack << first_num + second_num
     when :-
       @stack << first_num - second_num
     when :/
       @stack << first_num.fdiv(second_num)
     when :*
       @stack << first_num * second_num
     else
       @stack << first_num
       @stack << second_num
       "Invalid operation: #{op_symbol}"
     end
  end




end
